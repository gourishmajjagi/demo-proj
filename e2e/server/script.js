var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); // Body parser use JSON data

app.post('/Store', function(req, res) {
  var outputFilenameToStore;
  var dataToStore;
  if (req.query.TransactionData) {
    outputFilenameToStore = '../data/TransactionsData/transactions.json';
    dataToStore = req.query.TransactionData;
  } else if (req.query.ConnectorVariableData) {
    outputFilenameToStore = '../data/ConnectorVariableData/chargerprofile.json';
    dataToStore = req.query.ConnectorVariableData;
  } else if (req.query.BusData) {
    outputFilenameToStore = '../data/BusData/buses.json';
    dataToStore = req.query.BusData;
  } else {
    res.send('not saved as the params not matching with the desired values');
    return;
  }

  fs.writeFileSync(outputFilenameToStore, dataToStore); // write to the file system
  res.send('Saved to ' + outputFilenameToStore);
});

var port = 3000;
app.listen(port);
console.log('Express started on port %d ...', port);
