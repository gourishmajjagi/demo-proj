#!/bin/sh

# Set location of webbackend directory
BACKENDDIR="../server"

# Start webbackend application
echo -e "Starting the script"
cd $BACKENDDIR
node script.js
echo -e "script Started"
